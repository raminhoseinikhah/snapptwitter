    //
    //  CustomSearchView.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/8 AP.
    //

import UIKit

    // MARK: -
protocol CustomSearchViewDelegate {
    func clear()
    func textChanged(text: String?)
}


    // MARK: -
class CustomSearchView: UIView {
    
        // MARK: -
    var delegate: CustomSearchViewDelegate?
    private var searchTimer: Timer?
    
        // MARK: -
    private lazy var textField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        textField.returnKeyType = .done
        textField.becomeFirstResponder()
        textField.tintColor = .systemBlue
        textField.textColor = .white
        textField.textAlignment = .left
        textField.attributedPlaceholder = NSAttributedString(string: "Search Twitter", attributes: [.foregroundColor: UIColor.lightGray])
        return textField
    }()
    
    private lazy var cancelButton: UIButton = {
        let subview = UIButton()
        subview.translatesAutoresizingMaskIntoConstraints = false
        subview.setImage(UIImage(named: "clear"), for: .normal)
        subview.tintColor = .systemBlue
        subview.contentEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        subview.addTarget(self, action: #selector(clear), for: .touchUpInside)
        subview.isHidden = true
        return subview
    }()
    
    
        // MARK: -
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup() {
        backgroundColor = .clear
        clipsToBounds = true
        layer.cornerRadius = 4
        layer.borderWidth = 1
        layer.borderColor = UIColor.darkGray.cgColor
        
        addSubview(textField)
        textField.topAnchor.constraint(equalTo: topAnchor).isActive = true
        textField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        textField.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        addSubview(cancelButton)
        cancelButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        cancelButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        cancelButton.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        cancelButton.leadingAnchor.constraint(equalTo: textField.trailingAnchor).isActive = true
    }
    
    func resignResponder() {
        textField.resignFirstResponder()
    }
    
    @objc private func clear() {
        textField.text = nil
        delegate?.clear()
        cancelButton.isHidden = true
    }
    
    @objc private func textFieldDidChange() {
        searchTimer?.invalidate()
        searchTimer = nil
        cancelButton.isHidden = textField.text?.isEmpty ?? true
        if textField.text?.isEmpty ?? true {
            delegate?.textChanged(text: nil)
            return
        }
        searchTimer = Timer.scheduledTimer(timeInterval: 0.7, target: self, selector: #selector(search), userInfo: nil, repeats: false)
    }
    
    @objc private func search() {
        cancelButton.isHidden = textField.text?.isEmpty ?? true
        delegate?.textChanged(text: textField.text)
    }
    
}
