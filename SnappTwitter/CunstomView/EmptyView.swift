    //
    //  EmptyView.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/8 AP.
    //

import UIKit

@IBDesignable
class EmptyView: UIControl {
    
        // MARK: -
    var emptyMessage: String? {
        didSet {
            isHidden = false
            button.isHidden = true
            textLablel.text = emptyMessage
            textLablel.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        }
    }
    
    var error: String? {
        didSet {
            isHidden = false
            button.isHidden = false
            textLablel.text = error
            textLablel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        }
    }
    
    
        //MARK: -
    private lazy var textLablel: UILabel = {
        let lablel = UILabel()
        lablel.textAlignment = .center
        lablel.translatesAutoresizingMaskIntoConstraints = false
        lablel.textColor = .white
        lablel.numberOfLines = 5
        return lablel
    }()
    
    private lazy var button: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.contentEdgeInsets = UIEdgeInsets(top: 8, left: 40, bottom: 8, right: 40)
        button.addTarget(self, action: #selector(tapped), for: .touchUpInside)
        button.setTitle("retry", for: .normal)
        button.layer.cornerRadius =  4
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.white.cgColor
        button.setTitleColor(UIColor.white, for: .normal)
        return button
    }()
    
        // MARK: -
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        self.setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup() {
        backgroundColor = .black
        clipsToBounds = true
        isUserInteractionEnabled = true
        
        addSubview(textLablel)
        addSubview(button)
        
        textLablel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        textLablel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        textLablel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -44).isActive = true
        
        button.topAnchor.constraint(equalTo: textLablel.bottomAnchor, constant: 12).isActive = true
        button.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    }
    
    @objc private func tapped() {
        sendActions(for: .touchUpInside)
        isHidden = true
    }
}
