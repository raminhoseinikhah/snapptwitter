    //
    //  ImageContainerView.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/8 AP.
    //

import UIKit

@IBDesignable
class ImageContainerView: UIStackView {
    
        // MARK: -
    private let colors: [UIColor] = [.cyan, .yellow, .green, .magenta]
    
        // MARK: -
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        self.setup()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        backgroundColor = .lightGray
        layer.cornerRadius = 4
        layer.masksToBounds = true
        backgroundColor = .black
        clipsToBounds = true
        alignment = .fill
        axis = .vertical
        spacing = 2
        distribution = .fillEqually
    }
    
        // MARK: -
    var height: CGFloat {
        guard let medias = medias, !medias.isEmpty else {
            return 0
        }
        if medias.count == 1 {
            return bounds.size.width * medias.first!.ratio
        }
        return bounds.size.width * 0.66
    }
    
    var medias: [Media]? {
        didSet {
            subviews.forEach { $0.removeFromSuperview() }
            guard let medias = medias, !medias.isEmpty else {
                isHidden = true
                return
            }
            isHidden = false
            if medias.count == 1 {
                let imageView = createImageView(media: medias.first!, color: colors[0])
                addSubview(imageView)
                imageView.fillSuperview()
                return
            }
            let rows = (medias.count == 2) ? 1 : 2
            let columns = Int(round(Double(medias.count) / Double(rows)))
            
            for row in 0 ..< rows {
                let stackView = UIStackView()
                stackView.backgroundColor = .clear
                stackView.translatesAutoresizingMaskIntoConstraints = false
                stackView.alignment = .fill
                stackView.axis = .horizontal
                stackView.spacing = 2
                stackView.distribution = .fillEqually
                addArrangedSubview(stackView)
                
                fillHorizontalStackView(stackView: stackView, medias: medias, row: row, rowCount: rows, columnCount: columns)
            }
        }
    }
    
    private func fillHorizontalStackView(stackView: UIStackView, medias: [Media], row: Int, rowCount: Int, columnCount: Int) {
        for column in 0 ..< columnCount {
            let index = (row * columnCount) + column
            if index < medias.count {
                let imageView = createImageView(media: medias[index], color: colors[index % colors.count])
                stackView.addArrangedSubview(imageView)
            }
        }
    }
    
    private func createImageView(media: Media, color: UIColor) -> UIImageView {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.backgroundColor = color.withAlphaComponent(0.45)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.setImage(urlString: media.imageURL)
        return imageView
        
    }
}
