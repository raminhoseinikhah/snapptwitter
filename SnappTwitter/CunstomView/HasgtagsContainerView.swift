    //
    //  HasgtagsContainerView.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/9 AP.
    //

import UIKit

protocol Tag {
    var title: String? { get }
}


    // MARK: -
@IBDesignable
class HasgtagsContainerView: UIStackView {
    
        // MARK: -
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        self.setup()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        backgroundColor = .clear
        layer.cornerRadius = 4
        layer.masksToBounds = true
        backgroundColor = .black
        clipsToBounds = true
        alignment = .fill
        axis = .vertical
        spacing = 4
        distribution = .fillEqually
    }
    
    
    func add(tags: [Tag]?) {
        subviews.forEach { $0.removeFromSuperview() }
        for tag in tags ?? [] {
            let label = label()
            label.text = tag.title
            addArrangedSubview(label)
        }
    }
    
    private func label() -> UILabel {
        let label = UILabel()
        label.textColor = .systemBlue
        label.textAlignment = .left
        return label
    }
}
