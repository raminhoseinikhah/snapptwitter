//
//  RoundImageView.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/8 AP.
//

import UIKit

@IBDesignable
class RoundImageView: UIImageView {
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override public init(frame: CGRect){
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override var bounds: CGRect {
        didSet {
            self.layer.cornerRadius = bounds.height/2
        }
    }
    
    private func setup() {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = bounds.height/2
    }
}
