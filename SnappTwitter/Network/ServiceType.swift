    //
    //  ServiceType.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/7 AP.
    //

protocol ServiceType {
    
    func searchTweet(query: String, nextToken: String?, oldestID: String?, success: ((_: GenericResponse<[Tweet]>) -> Void)?,
                     error: ((_: Error) -> Void)?)
    
    func singleTweet(tweetID: String, success: ((_: GenericResponse<Tweet>) -> Void)?, error: ((_: Error) -> Void)?)
    
    func recentTweetsReplayTo(tweetID: String, nextToken: String?, oldestID: String?, success: ((_: GenericResponse<[Tweet]>) -> Void)?,
                              error: ((_: Error) -> Void)?)
    
}
