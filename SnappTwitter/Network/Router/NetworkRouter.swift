//
//  NetworkRouter.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/7 AP.
//

import Alamofire

protocol NetworkRouter {
    var method: HTTPMethod { get }
    var encoding: ParameterEncoding {get }
    var url: String { get }
    var params: [String: Any]? { get }
}
