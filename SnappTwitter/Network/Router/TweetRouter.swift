    //
    //  TweetRouter.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/7 AP.
    //

import Alamofire

enum TweetRouter {
    
    case search(query: String, nextToken: String?, oldestID: String?)
    
    case singleTweet(tweetID: String)
    
    case recentTweetsReplayTo(tweetID: String, nextToken: String?, oldestID: String?)
    
}


    // MARK: -
extension TweetRouter: NetworkRouter {
    
    var url: String {
        switch self {
            case .search, .recentTweetsReplayTo:
                return "\(APIService.baseURL)/tweets/search/recent"
            case .singleTweet(let tweetID):
                return "\(APIService.baseURL)/tweets/\(tweetID)"
        }
    }
    
    var params: [String: Any]? {
        switch self {
            case let .search(query, nextToken, oldestID):
                var params: [String: Any] = ["tweet.fields": "context_annotations,created_at,entities,geo,id,referenced_tweets,source,text,public_metrics",
                                             "expansions": "author_id,attachments.media_keys",
                                             "media.fields": "duration_ms,height,preview_image_url,type,url,width",
                                             "user.fields": "name,profile_image_url,username",
                                             "query": query]
                if let nextToken = nextToken {
                    params["next_token"] = nextToken
                }
                if let oldestID = oldestID {
                    params["until_id"] = oldestID
                }
                return params
                
            case let .recentTweetsReplayTo(query, nextToken, oldestID):
                var params: [String: Any] = ["tweet.fields": "context_annotations,created_at,entities,geo,id,referenced_tweets,source,text,public_metrics",
                                             "expansions": "author_id,attachments.media_keys",
                                             "media.fields": "duration_ms,height,preview_image_url,type,url,width",
                                             "user.fields": "name,profile_image_url,username",
                                             "query": "conversation_id:\(query)"]
                if let nextToken = nextToken {
                    params["next_token"] = nextToken
                }
                if let oldestID = oldestID {
                    params["until_id"] = oldestID
                }
                return params
                
            case .singleTweet:
                return ["tweet.fields": "context_annotations,created_at,entities,geo,id,referenced_tweets,source,text,public_metrics",
                        "expansions": "author_id,attachments.media_keys",
                        "media.fields": "duration_ms,height,preview_image_url,type,url,width",
                        "user.fields": "name,profile_image_url,username"]
        }
    }
    
    var method: HTTPMethod {
        .get
    }
    
    var encoding: ParameterEncoding {
        URLEncoding.default
    }
}
