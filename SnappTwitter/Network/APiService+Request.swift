    //
    //  APiService+Request.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/7 AP.
    //

extension APIService: ServiceType {
    
    func searchTweet(query: String, nextToken: String?, oldestID: String?, success: ((GenericResponse<[Tweet]>) -> Void)?,
                     error: ((Error) -> Void)?) {
        response(router: TweetRouter.search(query: query, nextToken: nextToken, oldestID: oldestID), success: success, error: error)
    }
    
    func singleTweet(tweetID: String, success: ((GenericResponse<Tweet>) -> Void)?, error: ((Error) -> Void)?) {
        response(router: TweetRouter.singleTweet(tweetID: tweetID), success: success, error: error)
    }
    
    func recentTweetsReplayTo(tweetID: String, nextToken: String?, oldestID: String?, success: ((_: GenericResponse<[Tweet]>) -> Void)?,
                              error: ((Error) -> Void)?) {
        response(router: TweetRouter.recentTweetsReplayTo(tweetID: tweetID, nextToken: nextToken, oldestID: oldestID), success: success, error: error)
    }
}
