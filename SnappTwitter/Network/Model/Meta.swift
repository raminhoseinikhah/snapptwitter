//
//  Meta.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/7 AP.
//

struct Meta: Decodable {
    var newestID: String?
    var oldestID: String?
    var resultCount: Int?
    var nextToken: String?
   
    enum CodingKeys: String, CodingKey {
        case newestID = "newest_id"
        case oldestID = "oldest_id"
        case resultCount = "result_count"
        case nextToken = "next_token"
    }
}
