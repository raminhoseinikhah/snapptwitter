//
//  GenericResponse.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/7 AP.
//

struct GenericResponse<T: Decodable>: Decodable {
    
    var data: T?
    var includes: Include?
    var meta: Meta?
    
    private enum CodingKeys: String, CodingKey {
        case data
        case includes
        case meta
    }
}
