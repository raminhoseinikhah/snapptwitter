//
//  ServerError.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/9 AP.
//

struct ServerError: Decodable {

    var title: String?
    var detail: String?
    
    private enum CodingKeys: String, CodingKey {
        case title
        case detail
    }
}
