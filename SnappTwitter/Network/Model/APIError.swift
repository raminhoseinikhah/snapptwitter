    //
    //  APIError.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/7 AP.
    //


enum APIError: Error {
    
    case noNetwork
    case unknownError(detail: String?)
    case unauthorized(detail: String?)
    
    var message: String? {
        switch self {
            case .noNetwork:
                return "no network connectivity"
            case .unknownError(let detail):
                return detail ?? "Something went wrong. Try reloading."
            case .unauthorized(let detail):
                return detail ?? "Unauthorized"
        }
    }
}
