    //
    //  APIService.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/7 AP.
    //

import Alamofire

final class APIService {
    
    static let baseURL = "https://api.twitter.com/2" // TODO
    
    init() {
        URLSession.shared.configuration.timeoutIntervalForRequest = 30
        URLSession.shared.configuration.requestCachePolicy = .reloadIgnoringCacheData
    }
    
    private var headers: HTTPHeaders {
        var headers = HTTPHeaders()
        headers["Content-Type"] = "application/json"
        headers ["Authorization"] = "Bearer AAAAAAAAAAAAAAAAAAAAAK%2BeUgEAAAAAcBPofcvampMachJq8uDh%2F2DrXsA%3DSOlOZ1jP9FAeqCvoqx6Mqc9B87r0OgjZPpOsYJexFzJpUQEfLw"
        return headers
    }
    
    private var isInternetAvailable: Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}


    // MARK: -
extension APIService {
    
    func response<T: Decodable>(router: NetworkRouter, success: ((_: GenericResponse<T>) -> Void)?, error: ((_: Error) -> Void)? ) {
        guard isInternetAvailable else {
            error?(APIError.noNetwork)
            return
        }
        Session.default.request(router.url, method: router.method, parameters: router.params, encoding: router.encoding, headers: headers)
            .validate(resultValidator)
            .responseDecodable(of: GenericResponse<T>.self, completionHandler: { response in
                if let value = response.value {
                    success?(value)
                } else {
                    if let err = response.error?.asAFError?.underlyingError as? APIError {
                        error?(err)
                    } else {
                        error?(APIError.unknownError(detail: nil))
                    }
                }
            })
    }
}


    // MARK: -  Error Handling
extension APIService {
    
    func resultValidator(request: URLRequest?, response: HTTPURLResponse, data: Data?) -> Request.ValidationResult {
        guard !(200...299).contains(response.statusCode) else {
            return .success(())
        }
            // handle other errors if need
        var error: ServerError? = nil
        if let d = data {
            error = ServerError.object(data: d)
        }
        switch response.statusCode {
            case NSURLErrorTimedOut, NSURLErrorNetworkConnectionLost:
                return .failure(APIError.noNetwork)
            case 401:
                return .failure(APIError.unauthorized(detail: error?.detail))
//               and so ...
//                     ...
            default:
                return .failure(APIError.unknownError(detail: error?.detail))
        }
    }
}
