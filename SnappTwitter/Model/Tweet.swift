    //
    //  Tweet.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/7 AP.
    //

struct Tweet: Decodable {
    
    var id: String?
    var authorID: String?
    var source: String?
    var createdAt: String?
    var text: String?
    var referencedTweets: [ReferencedTweet]?
    var entities: Entity?
    var attachments: Attachment?
    var publicMetrics: PublicMetric?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case authorID = "author_id"
        case source
        case createdAt = "created_at"
        case text
        case referencedTweets = "referenced_tweets"
        case entities
        case attachments
        case publicMetrics = "public_metrics"
    }
}
