    //
    //  Mention.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/7 AP.
    //

struct Mention: Decodable {
    
    var username: String?
    
    private enum CodingKeys: String, CodingKey {
        case username
    }
}




    // MARK: -
extension Mention: Tag {
    
    var title: String? {
        if let username = username {
            return "#\(username)"
        }
        return nil
    }
}
