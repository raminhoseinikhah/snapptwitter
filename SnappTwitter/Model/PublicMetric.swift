//
//  PublicMetric.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/8 AP.
//

struct PublicMetric: Decodable {
      
    var retweetCount: Int?
    var replyCount: Int?
    var likeCount: Int?
    var quoteCount: Int
    
    private enum CodingKeys: String, CodingKey {
        case retweetCount = "retweet_count"
        case replyCount = "reply_count"
        case likeCount = "like_count"
        case quoteCount = "quote_count"
    }
}
