//
//  Include.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/7 AP.
//


struct Include: Decodable {
    
    var users: [User]?
    var media: [Media]?
   
    private enum CodingKeys: String, CodingKey {
        case users
        case media
    }
    
    
    func user(for authorID: String?) -> User? {
        users?.first { $0.id == authorID }
    }
    
    func media(for mediaKeys: [String]?) -> [Media]? {
        media?.filter({ media in
            mediaKeys?.contains{ media.mediaKey ?? "" == $0 } ?? false
        })
    }
}
