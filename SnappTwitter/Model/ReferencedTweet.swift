//
//  ReferencedTweet.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/7 AP.
//

struct ReferencedTweet: Decodable {
    
    var id: String?
    var type:TweetType?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case type
    }
}
