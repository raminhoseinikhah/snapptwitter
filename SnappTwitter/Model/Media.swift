    //
    //  Media.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/7 AP.
    //

import UIKit

struct Media: Decodable {
    
    var url: String?
    var previewImageURL: String?
    var mediaKey: String?
    var height: Int?
    var width: Int?
    var type: MediaType?
    
    private enum CodingKeys: String, CodingKey {
        case url
        case previewImageURL = "preview_image_url"
        case mediaKey = "media_key"
        case height
        case width
        case type
    }
    
    var ratio: CGFloat {
        guard let h = height, let w = width, w > 0 else {
            return  0.5
        }
        return CGFloat(h)/CGFloat(w)
    }
    
    var imageURL: String? {
        if type == .video {
            return previewImageURL
        }
        return url
    }
}
