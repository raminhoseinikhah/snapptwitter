//
//  Attachment.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/7 AP.
//

struct Attachment: Decodable {
    
    var mediaKeys: [String]?
    
    private enum CodingKeys: String, CodingKey {
        case mediaKeys = "media_keys"
    }
}
