//
//  Entity.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/7 AP.
//

struct Entity: Decodable {
    
    var hashtags: [Hashtag]?
    var mentions: [Mention]?
    
    private enum CodingKeys: String, CodingKey {
        case hashtags
        case mentions
    }
}
