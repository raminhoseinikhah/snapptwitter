//
//  User.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/7 AP.
//

struct User: Decodable {
    
    var id: String?
    var name: String?
    var username: String?
    var imageURL: String?
   
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case username
        case imageURL = "profile_image_url"
    }
}
