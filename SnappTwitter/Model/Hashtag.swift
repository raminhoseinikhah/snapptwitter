//
//  Hashtag.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/7 AP.
//

struct Hashtag: Decodable {
    
    var tag: String?
    
    private enum CodingKeys: String, CodingKey {
        case tag
    }
}


// MARK: -
extension Hashtag: Tag {
    
    var title: String? {
        if let tag = tag {
            return "#\(tag)"
        }
        return nil
    }
}
