    //
    //  AppCoordinator.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/10 AP.
    //

import UIKit

class AppCoordinator {
    
    private let window: UIWindow
    private let service: ServiceType
    private let navigationController: UINavigationController
    
    // MARK: -
    init(window: UIWindow, service: ServiceType) {
        self.window = window
        self.service = service
        navigationController = UINavigationController()
    }
    
    func start() {
        var vc = MainViewController()
        
        let viewModel = MainViewModel(service: service)
        viewModel.showTweetDetail = { [weak self] tweetID in
            guard let self = self else { return }
            self.showTweetDetail(tweetID: tweetID)
        }
        
        vc.bindViewModel(to: viewModel)
        navigationController.setViewControllers([vc], animated: false)
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    
    private func showTweetDetail(tweetID: String) {
        var vc = TweetDetailViewController()
        let viewModel = TweetDetailViewModel(tweetID: tweetID, service: service)
        vc.bindViewModel(to: viewModel)
        navigationController.pushViewController(vc, animated: true)
    }
}
