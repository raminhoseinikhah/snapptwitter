    //
    //  UIImageView.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/8 AP.
    //

import UIKit
import SDWebImage

extension UIImageView {
    
    func setImage(urlString: String?, placeholder: UIImage? = nil) {
        self.image = placeholder
        setImageUrl(url: URL(string: urlString ?? ""), placeholder: placeholder)
    }
    
    private func setImageUrl(url: URL?, placeholder: UIImage?) {
        guard let imageURL = url else {
            return
        }
        sd_setImage(with: imageURL, placeholderImage: placeholder) { [weak self] (image, error, _, _) in
            if error != nil || image == nil {
                self?.image = placeholder
            } else {
                self?.image = image
            }
        }
    }
}
