//
//  DateFormatter.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/8 AP.
//

import UIKit

extension ISO8601DateFormatter {

    static let iso = ISO8601DateFormatter(formatOptions: [.withInternetDateTime, .withFractionalSeconds])
    
    convenience init(formatOptions: Options) {
        self.init()
        self.formatOptions = formatOptions
    }
}


extension DateFormatter {
    
    static let twitter = DateFormatter(locale: Locale(identifier: "en") )
    
    convenience init(locale: Locale) {
        self.init()
        self.locale = locale
    }
}
