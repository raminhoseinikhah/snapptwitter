    //
    //  Decodable.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/9 AP.
    //

import UIKit

extension Decodable {
    
    static func object(data: Data) -> Self? {
        try? JSONDecoder().decode(Self.self, from: data)
    }
}
