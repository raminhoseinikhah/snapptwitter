    //
    //  UITableView.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/8 AP.
    //

import UIKit

    // MARK: -
extension UITableViewCell: ReusableView {}

extension UITableViewCell: NibLoadableView {}

    // MARK: -
extension UITableViewHeaderFooterView: ReusableView { }

extension UITableViewHeaderFooterView: NibLoadableView { }



    // MARK: -
extension UITableView {
    
    func register<T: UITableViewCell>(_ :T.Type) {
        register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func registerNib<T: UITableViewCell>(_: T.Type) {
        let nib = UINib(nibName: T.nibName, bundle: nil)
        register(nib, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func registerHeaderFooterView<T: UITableViewHeaderFooterView>(_: T.Type) {
        register(UINib(nibName: T.nibName, bundle: nil), forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
    }
    
    
        // MARK: -
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>(_ type: T.Type) -> T {
        dequeueReusableHeaderFooterView(withIdentifier: type.reuseIdentifier) as! T
    }
    
    func dequeueReusableCell<T: UITableViewCell>(_ type: T.Type, for indexPath: IndexPath) -> T {
        dequeueReusableCell(withIdentifier: type.reuseIdentifier, for: indexPath) as! T
    }
}


    // MARK: -
extension UITableView {
    
    func setLoading(width: CGFloat) {
        let view = UIView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: 56)))
        view.backgroundColor = .clear
        let spinner = UIActivityIndicatorView()
        spinner.color = UIColor.white
        spinner.center = view.center
        view.addSubview(spinner)
        spinner.startAnimating()
        tableFooterView = view
    }
}
