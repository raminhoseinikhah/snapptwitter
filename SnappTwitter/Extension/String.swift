    //
    //  String.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/8 AP.
    //

import UIKit

extension String {
    
    func getAttributedString(font: UIFont, textColor: UIColor = .white, lineSpacing: CGFloat = 6) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let str = replacingFirstOccurrence(of: "RT ", with: "Replying To ")
        let attrStr = NSMutableAttributedString(string: str, attributes: [.font: font, .foregroundColor: textColor, .paragraphStyle: paragraphStyle])
        
        let searchPattern = "[@#]\\w+"
        var ranges: [NSRange] = [NSRange]()
        let regex = try! NSRegularExpression(pattern: searchPattern, options: [])
        ranges = regex.matches(in: attrStr.string, options: [], range: NSMakeRange(0, attrStr.string.count)).map {$0.range}
        
        for range in ranges {
            attrStr.addAttribute(.foregroundColor, value: UIColor.systemBlue, range: NSRange(location: range.location, length: range.length))
        }
        return attrStr
    }
    
    
    func replacingFirstOccurrence(of target: String, with replacement: String) -> String {
        guard let range = self.range(of: target) else { return self }
        return replacingCharacters(in: range, with: replacement)
    }
}



    // MARK: -
extension String {
    
    var trim: String {
        return trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var date: String? {
        if let d = ISO8601DateFormatter.iso.date(from: self) {
            DateFormatter.twitter.dateFormat = "EE d MMM"
            return DateFormatter.twitter.string(from: d)
        }
        return nil
    }
    
    var fullDate: String? {
        if let d = ISO8601DateFormatter.iso.date(from: self) {
            DateFormatter.twitter.dateFormat = "HH:mm .  EE d MMM"
            return DateFormatter.twitter.string(from: d)
        }
        return nil
    }
}
