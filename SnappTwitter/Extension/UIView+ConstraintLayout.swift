//
//  UIView+ConstraintLayout.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/8 AP.
//

import UIKit

extension UIView {
    
    func fillSuperview(top: CGFloat = 0, left: CGFloat = 0, bottom: CGFloat = 0, right: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        guard let superview = superview else {
            return
        }
        topAnchor.constraint(equalTo: superview.topAnchor, constant: top).isActive = true
        leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: left).isActive = true
        bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -bottom).isActive = true
        trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -right).isActive = true
    }
    
    func fillSafeAreaLayoutGuide(top: CGFloat = 0, left: CGFloat = 0, bottom: CGFloat = 0, right: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        guard let superview = superview else {
            return
        }
        topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor, constant: top).isActive = true
        leadingAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.leadingAnchor, constant: left).isActive = true
        bottomAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.bottomAnchor, constant: -bottom).isActive = true
        trailingAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.trailingAnchor, constant: -right).isActive = true
    }
}

