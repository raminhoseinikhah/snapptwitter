    //
    //  ReusableView.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/9 AP.
    //

import UIKit

protocol ReusableView: AnyObject {}

extension ReusableView where Self: UIView {
    
    static var reuseIdentifier: String {
        String(describing: self)
    }
}

    // MARK: -
protocol NibLoadableView: AnyObject {}

extension NibLoadableView where Self: UIView {
    static var nibName: String {
        String(describing: self)
    }
}

