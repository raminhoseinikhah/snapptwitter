//
//  BindableType.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/7 AP.
//


import UIKit

    /// Each view controller conforming to the BindableType protocol will declare a viewModel property and provide a bindViewModel() method to be called once the viewModel property is assigned. This method will connect UI elements to observables and actions in the view model.
protocol BindableType {
    associatedtype ViewModelType
    var viewModel: ViewModelType! { get set }
    func bindViewModel()
}


    //You want the viewModel property to be assigned to your view controller as soon as possible, but bindViewModel() must be invoked only after the view has been loaded. The reason is that your bindViewModel() will connect UI elements that need to be present.

    // VM is injected into the VC’s var viewModel , loads the view (if not already loaded) so that the initial UI is ready to be updated by an initial output from the VM if any, and finally makes the VC bind to the transported VM

extension BindableType where Self: UIViewController {
    
    mutating func bindViewModel(to model: Self.ViewModelType) {
        viewModel = model
        loadViewIfNeeded() /// Loads viewController’s view if it has not yet been loaded. Calling this method loads view controller’s view from its storyboard file, or creates the view as needed based on the established rules.
        bindViewModel()
    }
}
