    //
    //  TweetDetailViewModel.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/9 AP.
    //

class TweetDetailViewModel {
    
        // MARK: -
    private let service: ServiceType
    private let tweetID: String
    
    private(set) var tweet: TweetTableViewHeaderViewModel?
    private var list: [TweetViewModel] = []
    private var meta: Meta?
    private var inloading = false {
        didSet {
            if inloading {
                showLoading?()
            } else {
                stopLoading?()
            }
        }
    }
    
        // MARK: - Outputs:
    var showLoading: (() -> Void)?
    var stopLoading: (() -> Void)?
    var reloadData: (() ->Void)?
    var error: ((_: String) -> Void)?
    
        // MARK: -
    init(tweetID: String, service: ServiceType) {
        self.tweetID = tweetID
        self.service = service
    }
    
    func getTweet() {
        singleTweet()
    }
    
    func retry() {
        singleTweet()
    }
    
    func loadMore() {
        if meta?.nextToken != nil, meta?.oldestID != nil, !inloading {
            recentTweetsReplayTo()
        }
    }
}


    // MARK: -  for TableView
extension TweetDetailViewModel {
    
    var numberOfRowsInSection: Int {
        list.count
    }
    
    func item(for row: Int) -> TweetViewModel {
        list[row]
    }
}


    // MARK: - Service
extension TweetDetailViewModel {
    
    private func singleTweet() {
        inloading = true
        service.singleTweet(tweetID: tweetID) { [weak self] response in
            guard let self = self else { return }
            self.tweet = TweetTableViewHeaderViewModel.mapToTweetViewModel(response: response)
            self.reloadData?()
            self.recentTweetsReplayTo()
        } error: { [weak self] err in
            guard let self = self else { return }
            self.error?((err as? APIError)?.message ?? "unknown Error")
            self.inloading = false
        }
    }
    
    private func recentTweetsReplayTo() {
        inloading = true
        service.recentTweetsReplayTo(tweetID: tweetID, nextToken: meta?.nextToken, oldestID: meta?.oldestID) { [weak self] response in
            guard let self = self else { return }
            
            let list = TweetViewModel.mapToTweetViewModels(response: response)
            self.list.append(contentsOf: list)
            
            self.meta = response.meta
            self.reloadData?()
            self.inloading = false
        } error: { [weak self] err in
            guard let self = self else { return }
            self.inloading = false
        }
    }
}
