    //
    //  TweetTableViewHeaderViewModel.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/10 AP.
    //


struct TweetTableViewHeaderViewModel {
    
        // MARK: -
    private let tweet: Tweet
    private let user: User?
    let media: [Media]?
    
        // MARK: -
    init(tweet: Tweet, user: User?, media: [Media]?) {
        self.tweet = tweet
        self.user = user
        self.media = media
    }
    
    var tweetID: String? {
        tweet.id
    }
    
    var imageURL: String? {
        user?.imageURL
    }
    
    var name: String? {
        user?.name
    }
    
    var username: String? {
        if let username = user?.username {
            return "@\(username)"
        }
        return nil
    }
    
    var text: String? {
        tweet.text?.trim
    }
    
    var tags: [Tag] {
        var tags: [Tag] = []
        tags.append(contentsOf: tweet.entities?.hashtags ?? [])
        tags.append(contentsOf: tweet.entities?.mentions ?? [])
        return tags
    }
    
    var replyCount: Int {
        tweet.publicMetrics?.replyCount ?? 0
    }
    
    var retweetCount: Int {
        tweet.publicMetrics?.retweetCount ?? 0
    }
    
    var likeCount: Int {
        tweet.publicMetrics?.likeCount ?? 0
    }
    
    var quoteCount: Int {
        tweet.publicMetrics?.quoteCount ?? 0
    }
    
    var fullDate: String? {
        tweet.createdAt?.fullDate
    }
    
    var source: String? {
        tweet.source
    }
}


    // MARK: -
extension TweetTableViewHeaderViewModel {
    
    static func mapToTweetViewModel(response: GenericResponse<Tweet>) -> TweetTableViewHeaderViewModel? {
        guard let tweet = response.data else {
            return nil
        }
        let user = response.includes?.user(for: tweet.authorID)
        let media = response.includes?.media(for: tweet.attachments?.mediaKeys)
        return .init(tweet: tweet, user: user, media: media)
    }
}
