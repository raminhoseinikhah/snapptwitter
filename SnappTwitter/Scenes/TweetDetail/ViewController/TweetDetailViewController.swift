    //
    //  TweetDetailViewController.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/9 AP.
    //

import UIKit

class TweetDetailViewController: UIViewController, BindableType {
    
        // MARK: -
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.separatorColor = .lightGray
        tableView.separatorStyle = .singleLine
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.tableFooterView?.backgroundColor = .clear
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.tableHeaderView?.backgroundColor = .clear
        tableView.contentInset = .zero
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.keyboardDismissMode = .onDrag
        tableView.estimatedSectionHeaderHeight = 400
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 400
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerHeaderFooterView(TweetTableViewHeaderView.self)
        tableView.registerNib(TweetTableViewCell.self)
        return tableView
    }()
    
    private lazy var emptyView: EmptyView = {
        let subview = EmptyView()
        subview.isHidden = true
        return subview
    }()
    
    
        // MARK: -
    var viewModel: TweetDetailViewModel!
    
    
        // MARK: -
    override func loadView() {
        super.loadView()
        view.addSubview(tableView)
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8).isActive = true
        
        view.addSubview(emptyView)
        emptyView.leadingAnchor.constraint(equalTo: tableView.leadingAnchor).isActive = true
        emptyView.trailingAnchor.constraint(equalTo: tableView.trailingAnchor).isActive = true
        emptyView.topAnchor.constraint(equalTo: tableView.topAnchor).isActive = true
        emptyView.bottomAnchor.constraint(equalTo: tableView.bottomAnchor).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        tableView.delegate = self
        tableView.dataSource = self
        emptyView.addTarget(self, action: #selector(retry), for: .touchUpInside)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func bindViewModel() {
        viewModel.reloadData = { [weak self] in
            guard let self = self else { return }
            self.tableView.reloadData()
        }
        
        viewModel.showLoading = { [weak self] in
            guard let self = self else { return }
            self.view.layoutIfNeeded()
            self.tableView.setLoading(width: self.tableView.bounds.width)
        }
        
        viewModel.stopLoading = { [weak self] in
            guard let self = self else { return }
            self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 1, height: CGFloat.leastNormalMagnitude))
        }
        
        viewModel.error = { [weak self] err in
            guard let self = self else { return }
            self.emptyView.error = err
        }
        
        viewModel.getTweet()
    }
    
    
    @objc private func retry() {
        viewModel.retry()
    }
}



    // MARK: -
extension TweetDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let tweet = viewModel.tweet else {
            return nil
        }
        let header = tableView.dequeueReusableHeaderFooterView(TweetTableViewHeaderView.self)
        header.bindViewModel(to: tweet)
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(TweetTableViewCell.self, for: indexPath)
        cell.bindViewModel(to: viewModel.item(for: indexPath.row))
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if bottomEdge >= scrollView.contentSize.height {
            viewModel.loadMore()
        }
    }
}
