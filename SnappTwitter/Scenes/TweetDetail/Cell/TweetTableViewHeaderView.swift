    //
    //  TweetTableViewHeaderView.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/9 AP.
    //

import UIKit

class TweetTableViewHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var parentStackView: UIStackView!
    
    @IBOutlet weak var userImageView: RoundImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var hasgtagStackView: HasgtagsContainerView!
    
    @IBOutlet weak var desLabel: UILabel!
    
    @IBOutlet weak var imageContainerView: ImageContainerView!
    @IBOutlet weak var imageContaionerHeightCons: NSLayoutConstraint!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    
    @IBOutlet weak var retweetCountLabel: UILabel!
    @IBOutlet weak var quoteCounLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    
    
        // MARK: -
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = .black
        parentStackView.backgroundColor = .black
    }
    
    
        // MARK: -
    func bindViewModel(to model: TweetTableViewHeaderViewModel) {
        userImageView.setImage(urlString: model.imageURL)
        nameLabel.text = model.name
        usernameLabel.text =  model.username
        
        hasgtagStackView.add(tags: model.tags)
        
        desLabel.attributedText =  model.text?.getAttributedString(font: UIFont.systemFont(ofSize: 14, weight: .regular))
        
        imageContainerView.medias = model.media
        imageContaionerHeightCons.constant = imageContainerView.height
        
        dateLabel.text = model.fullDate
        sourceLabel.text = model.source
        
        retweetCountLabel.text = "\(model.retweetCount)"
        quoteCounLabel.text = "\(model.quoteCount)"
        likeCountLabel.text = "\(model.likeCount)"
        
        contentView.layoutIfNeeded()
    }
}
