    //
    //  TweetTableViewCell.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/8 AP.
    //

import UIKit

class TweetTableViewCell: UITableViewCell {
    
        // MARK: -
    @IBOutlet weak var userImageView: RoundImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!

    @IBOutlet weak var desLabel: UILabel!
    
    @IBOutlet weak var hasgtagStackView: HasgtagsContainerView!
    
    @IBOutlet weak var imageContainerView: ImageContainerView!
    @IBOutlet weak var imageContaionerHeightCons: NSLayoutConstraint!
    
    @IBOutlet weak var replyButton: UIButton!
    @IBOutlet weak var retweetButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    
    
        // MARK: -
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        backgroundColor = .black
        contentView.backgroundColor = .black
    }
    
    
        // MARK: -
    func bindViewModel(to model: TweetViewModel) {
        userImageView.setImage(urlString: model.imageURL)
        nameLabel.text = model.name
       
        usernameLabel.text =  model.username
        timeLabel.text = model.date
        
        desLabel.attributedText =  model.text?.getAttributedString(font: UIFont.systemFont(ofSize: 14, weight: .regular))
                
        hasgtagStackView.add(tags: model.tags)
        
        replyButton.setTitle(model.replyText, for: .normal)
        retweetButton.setTitle(model.retweetText, for: .normal)
        likeButton.setTitle(model.likeText, for: .normal)
        
        imageContainerView.medias = model.media
        imageContaionerHeightCons.constant = imageContainerView.height
        
        contentView.layoutIfNeeded()
    }
}
