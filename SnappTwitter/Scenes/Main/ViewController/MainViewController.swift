    //
    //  MainViewController.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/7 AP.
    //

import UIKit

class MainViewController: UIViewController, BindableType {
    
        // MARK: -
    private lazy var searchView: CustomSearchView = {
        let subview = CustomSearchView()
        return subview
    }()
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.separatorColor = .lightGray
        tableView.separatorStyle = .singleLine
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.tableFooterView?.backgroundColor = .clear
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.tableHeaderView?.backgroundColor = .clear
        tableView.estimatedRowHeight = 400
        tableView.rowHeight = UITableView.automaticDimension
        tableView.keyboardDismissMode = .onDrag

        tableView.registerNib(TweetTableViewCell.self)
        return tableView
    }()
    private lazy var emptyView: EmptyView = {
        let subview = EmptyView()
        subview.isHidden = true
        return subview
    }()
    
        // MARK: -
    var viewModel: MainViewModel!
    
    
        // MARK: -
    override func loadView() {
        super.loadView()
        view.addSubview(searchView)
        searchView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        searchView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 12).isActive = true
        searchView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        searchView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        view.addSubview(tableView)
        tableView.leadingAnchor.constraint(equalTo: searchView.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: searchView.trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: searchView.bottomAnchor, constant: 12).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8).isActive = true
        
        view.addSubview(emptyView)
        emptyView.leadingAnchor.constraint(equalTo: tableView.leadingAnchor).isActive = true
        emptyView.trailingAnchor.constraint(equalTo: tableView.trailingAnchor).isActive = true
        emptyView.topAnchor.constraint(equalTo: tableView.topAnchor).isActive = true
        emptyView.bottomAnchor.constraint(equalTo: tableView.bottomAnchor).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Tweet"
        view.backgroundColor = .black
        tableView.delegate = self
        tableView.dataSource = self
        
        emptyView.addTarget(self, action: #selector(retry), for: .touchUpInside)
        
        searchView.delegate = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func bindViewModel() {
        viewModel.reloadData = { [weak self] in
            guard let self = self else { return }
            self.tableView.reloadData()
        }
        
        viewModel.showLoading = { [weak self] in
            guard let self = self else { return }
            self.tableView.setLoading(width: self.view.bounds.width)
        }
        
        viewModel.stopLoading = { [weak self] in
            guard let self = self else { return }
            self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 1, height: CGFloat.leastNormalMagnitude))
        }
        
        viewModel.error = { [weak self] err in
            guard let self = self else { return }
            self.searchView.resignResponder()
            self.emptyView.error = err
        }
        
        viewModel.emptyMessage = { [weak self] message in
            guard let self = self else { return }
            self.searchView.resignResponder()
            self.emptyView.emptyMessage = message
        }
    }
    
    @objc private func retry() {
        viewModel.retry()
    }
}


    // MARK: -
extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(TweetTableViewCell.self, for: indexPath)
        cell.bindViewModel(to: viewModel.item(for: indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchView.resignResponder()
        viewModel.didSelectRowAt(row: indexPath.row)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if bottomEdge >= scrollView.contentSize.height {
            viewModel.loadMore()
        }
    }
}


// MARK: -
extension MainViewController: CustomSearchViewDelegate {
    
    func clear() {
        viewModel.clear()
        emptyView.isHidden = true
    }
    
    func textChanged(text: String?) {
        emptyView.isHidden = true
        viewModel.search(query: text ?? "")
    }
}
