    //
    //  MainViewModel.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/7 AP.
    //


class MainViewModel {
    
        // MARK: -
    private let service: ServiceType
    
    private var list: [TweetViewModel] = []
    private var meta: Meta?
    private var query = ""
    private var inloading = false {
        didSet {
            if inloading {
                showLoading?()
            } else {
                stopLoading?()
            }
        }
    }
    
    
        // MARK: - Outputs:
    var showTweetDetail: ((_: String) -> Void)?
    
    var showLoading: (() -> Void)?
    var stopLoading: (() -> Void)?
    var reloadData: (() ->Void)?
    var error: ((_: String) -> Void)?
    var emptyMessage: ((_: String) -> Void)?
    
    
        // MARK: -
    init(service: ServiceType) {
        self.service = service
    }
    
    private func reset() {
        list.removeAll()
        meta = nil
        inloading = false
        reloadData?()
    }
    
    func retry() {
        search(query: query)
    }
    
    func search(query: String) {
        reset()
        self.query = query
        if !query.isEmpty {
            searchTweet()
        }
    }
    
    func loadMore() {
        if meta?.nextToken != nil, meta?.oldestID != nil, !inloading {
            searchTweet()
        }
    }
    
    func clear() {
        query = ""
        reset()
    }
}


    // MARK: -  for TableView
extension MainViewModel {
    
    var numberOfRowsInSection: Int {
        list.count
    }
    
    func item(for row: Int) -> TweetViewModel {
        list[row]
    }
    
    func didSelectRowAt(row: Int) {
        guard let tweetID = item(for: row).tweetID else { return }
        showTweetDetail?(tweetID)
    }
}


    // MARK: - Service
extension MainViewModel {
    
    private func searchTweet() {
        inloading = true
        service.searchTweet(query: query, nextToken: meta?.nextToken, oldestID: meta?.oldestID) { [weak self] response in
            guard let self = self else { return }
            if self.query.isEmpty {
                self.reset()
                return
            }
            let list = TweetViewModel.mapToTweetViewModels(response: response)
            self.list.append(contentsOf: list)
            
            self.meta = response.meta
            self.reloadData?()
            self.inloading = false
            if self.list.isEmpty {
                self.emptyMessage?("No results for \"\(self.query)\"")
            }
        } error: { [weak self] err in
            guard let self = self else { return }
            if self.query.isEmpty {
                self.reset()
                return
            }
            if self.list.isEmpty {
                self.error?((err as? APIError)?.message ?? "unknown Error")
            }
            self.inloading = false
        }
    }
}
