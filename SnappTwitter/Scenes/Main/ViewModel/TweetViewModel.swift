    //
    //  TweetViewModel.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/8 AP.
    //

struct TweetViewModel {
    
        // MARK: -
    private let tweet: Tweet
    private let user: User?
    let media: [Media]?
    
        // MARK: -
    init(tweet: Tweet, user: User?, media: [Media]?) {
        self.tweet = tweet
        self.user = user
        self.media = media
    }
    
    var tweetID: String? {
        tweet.id
    }
    
    var imageURL: String? {
        user?.imageURL
    }
    
    var name: String? {
        user?.name
    }
    
    var username: String? {
        if let username = user?.username {
            return "@\(username)"
        }
        return nil
    }
    
    var text: String? {
        tweet.text?.trim
    }
    
    var tags: [Tag] {
        var tags: [Tag] = []
        tags.append(contentsOf: tweet.entities?.hashtags ?? [])
        tags.append(contentsOf: tweet.entities?.mentions ?? [])
        return tags
    }
    
    var replyText: String? {
        if let count = tweet.publicMetrics?.replyCount, count > 0 {
            return "\(count)"
        }
        return nil
    }
    
    var retweetText: String? {
        if let count = tweet.publicMetrics?.retweetCount, count > 0 {
            return "\(count)"
        }
        return nil
    }
    
    var likeText: String? {
        if let count = tweet.publicMetrics?.likeCount, count > 0 {
            return "\(count)"
        }
        return nil
    }
    
    var date: String? {
        tweet.createdAt?.date
    }
}



    // MARK: -
extension TweetViewModel {
    
    static func mapToTweetViewModels(response: GenericResponse<[Tweet]>) -> [TweetViewModel] {
        guard let tweets = response.data else {
            return []
        }
        let list = tweets.map { tweet -> TweetViewModel in
            let user = response.includes?.user(for: tweet.authorID)
            let media = response.includes?.media(for: tweet.attachments?.mediaKeys)
            return .init(tweet: tweet, user: user, media: media)
        }
        return list
    }
}
