    //
    //  MediaType.swift
    //  SnappTwitter
    //
    //  Created by Ramin on 1401/3/7 AP.
    //

enum MediaType: Decodable {
    case photo
    case video
    case gif
    
    init(from decoder: Decoder) throws {
        let status = try? decoder.singleValueContainer().decode(String.self)
        switch status?.lowercased() {
            case "video":
                self = .video
            case "gif":
                self = .gif
            default:
                self = .photo
        }
    }
}
