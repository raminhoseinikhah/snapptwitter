//
//  TweetType.swift
//  SnappTwitter
//
//  Created by Ramin on 1401/3/7 AP.
//

enum TweetType: Decodable {
    case retweeted
    case repliedTo
    case quoted
    
    init(from decoder: Decoder) throws {
        let status = try? decoder.singleValueContainer().decode(String.self)
        switch status?.lowercased() {
            case "replied_to":
                self = .repliedTo
            case "quoted":
                self = .quoted
            default:
                self = .retweeted
        }
    }
}
