
# SnappTwitter

Snapp Twitter

## Coordinator pattern:

The coordinator pattern is a structural design pattern for organizing flow logic between view controllers.
                                                            
                                                            
## MVVM architecture:
                                                                
I use mvvm architecture in the project.
                                                            
                                                            
## TweetDetailViewController:
                                                                
For getting all replied to for one tweet, we need to use full-archive search API endpoint, but this endpoint is only available to Projects with Academic Research access.
                                                            
So I use Recent search endpoint for this purpose and as a result only show recently replied tweets.
                                                            
## Notice:
                                                                
In Twitter API v2, at this time it is not possible to fetch GIF and video URLs currently. So I only show image preview for video tweets.
                                                            
